import axios from 'axios';

const aInstance = axios.create({
    baseURL: 'https://rebrickable.com/api/v3/',
    headers: {
        'Authorization': 'key 9b8232a03ce0b003bb404ca9929ed8d2'
    }
});

export default aInstance;