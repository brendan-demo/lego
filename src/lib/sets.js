import querystring from 'querystring';
import aInstance from './rebrickable';

export function setSearch(query, sort) {
    return aInstance.get(`/lego/sets?${querystring.stringify(query)}`)
}