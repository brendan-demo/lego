import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// Bulmna anmd Buefy
import Buefy from 'buefy'
import './assets/sass/index.scss'
Vue.use(Buefy);

//Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faExternalLinkAlt)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
